from django.conf.urls import patterns, include, url
from dh5bp.urls import urlpatterns as dh5bp_urls
# from django.contrib import admin
# admin.autodiscover()

handler404 = 'dh5bp.views.page_not_found'
handler500 = 'dh5bp.view.server_error'

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'cpp_regex_tester.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^match/', include('website.urls')),
)
urlpatterns += dh5bp_urls
