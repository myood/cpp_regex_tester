cpp-regex-tester: online regex tester for C++11 and Boost.Regex
========================
Dependencies:
---
Python 2.7
---
Boost.Python 1.54
---
g++ 4.9
---
django 1.6
---
django-html5-boilerplate
---
fabric 1.7
---
