from django.conf.urls import patterns, url

from website import views

urlpatterns = patterns('',
    url(r'^(?P<text_to_process>.*)/(?P<regular_expression>.*)/$', views.homepage, name='homepage')
)